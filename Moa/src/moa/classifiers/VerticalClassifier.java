package moa.classifiers;

import java.util.ArrayList;
import com.yahoo.labs.samoa.instances.Instance;
import java.util.Collections;
import java.util.List;
import moa.core.Measurement;
import com.github.javacliparser.IntOption;
import com.github.javacliparser.StringUtils;

// @author Eduardo Ferreira
// @email eduferreiraj@gmail.com

public class VerticalClassifier extends AbstractClassifier {
    public class FeatureElement {
        double featureValue;
        int targetClass;
        int element_id;
        public FeatureElement(double featureValue, int targetClass, int element_id){
            this.featureValue = featureValue;
            this.targetClass = targetClass;
            this.element_id = element_id;
        }
        @Override
        public String toString() {
            return String.valueOf(featureValue);
        }
    }
    // Classe para passar uma função como parâmetro.
    public abstract class FunctionObject{
        public abstract double get(int index);
    }
    
    public class GetFeature extends FunctionObject{
        ArrayList<FeatureElement> list;
        public GetFeature(ArrayList<FeatureElement> al){
            list = al;
        }
        @Override
        public double get(int index) {
            return list.get(index).featureValue;
        }
    }
    
    
    ArrayList<ArrayList<FeatureElement>> featureMatrix;
    public IntOption batchSize = new IntOption(
            "batchSize",
            'b',
            "The size of the batch for each feature stored.",
            200, 0, Integer.MAX_VALUE);
    public IntOption getVotesRange = new IntOption(
            "votesRange",
            'v',
            "Range of the features considered in the predict step.",
            200, 0, Integer.MAX_VALUE);
    int nextInstance = -1; 
        public class Tuple<X, Y> { 
        public final X x; 
        public final Y y; 
        public Tuple(X x, Y y) { 
          this.x = x; 
          this.y = y; 
        } 
    }
    public class ItemCounter{
        public class Item{
            int id;
            int times;
            public Item(int id){
                this.id = id;
                times = 1;
            }
            public void inc(){
                times = times + 1;
            }
            public int get(){
                return times;
            }
        }
        
        public class GetItem extends FunctionObject{
            ArrayList<Item> list;
            public GetItem(ArrayList<Item> al){
                list = al;
            }
            @Override
            public double get(int index) {
                return list.get(index).id;
            }
        }
        
        ArrayList<Item> itens; 
        public ItemCounter(){
            itens = new ArrayList<>();
        }
        public void reset(){
            itens = new ArrayList<>();
        }
        public void increment(int element){
            boolean incremented = false;
            if(itens.isEmpty()){
                itens.add(new Item(element));
                return;
            }
            int pos = binarySearchPosition(new GetItem(itens), itens.size() - 1, element);
            try{
                if(itens.get(pos).id == element){
                    incremented = true;
                    itens.get(pos).inc();
                }
            } catch(Exception e){
                
            }
            if(!incremented){
                itens.add(pos, new Item(element));
            }
        }
        
        public ArrayList<Tuple<Integer,ArrayList<Integer>>> getLists(){
            ArrayList<Tuple<Integer,ArrayList<Integer>>> list = new ArrayList<>();
            boolean flowControl;
            Tuple<Integer, ArrayList<Integer>> newTuple;
            ArrayList<Integer> newList;
            for (Item item : itens) {
                flowControl = false;
                for (Tuple<Integer, ArrayList<Integer>> itemList : list) {
                    if(itemList.x == item.times){
                        flowControl = true;
                        itemList.y.add(item.id);
                        break;
                    }
                }
                if(!flowControl){
                    newList = new ArrayList<>();
                    newList.add(item.id);
                    newTuple = new Tuple(item.times,newList);
                    list.add(newTuple);                    
                }
            }
            ArrayList<Tuple<Integer,ArrayList<Integer>>> orderedList = new ArrayList<>();
            //Sort
            int listSize = list.size();
            for(int i = 0; i < listSize; i++){
                Tuple<Integer, ArrayList<Integer>> highTuple = list.get(0);
                for( Tuple<Integer, ArrayList<Integer>> itemList : list){
                    if(highTuple.x < itemList.x){
                        highTuple = itemList;
                    } 
                }
                list.remove(highTuple);
                orderedList.add(highTuple);
            }
            return orderedList;
        }
    } 
    public ArrayList<Double> entropy(ArrayList<List<Integer>> votes, int class_size){
        ArrayList<Double> entropy = new ArrayList<>();
        ItemCounter counter = new ItemCounter();
        double temp_entropy, sum_entropy = 0.0;
        for(List<Integer> featureRange : votes){
            temp_entropy = 0.0;
            for(Integer vote : featureRange){
                counter.increment(vote);
            }
            ArrayList<Tuple<Integer,ArrayList<Integer>>> presenceList = counter.getLists();
            for(Tuple<Integer,ArrayList<Integer>> presence : presenceList){
                for(Integer class_qnt : presence.y){
                    temp_entropy = - (double)presence.x/(double)featureRange.size();
                    sum_entropy = sum_entropy + temp_entropy * (Math.log10((double)presence.x/(double)featureRange.size()) / Math.log10(2.));
                }
            }
            entropy.add(sum_entropy / (Math.log10((double)class_size) / Math.log10(2.)));
            counter.reset();
        }
        return entropy;
    }
    @Override // Testar
    public double[] getVotesForInstance(Instance inst) {
        //System.out.println("GetVotes");
        //System.out.print("Feature Matrix: ");
        //System.out.println(featureMatrix);
        ArrayList<List<Integer>> votes;
        double[] out = new double[inst.numClasses()];
        for(int i = 0; i < inst.numClasses(); i++)
            out[i] = 0.0;
        if(featureMatrix.isEmpty())
            return out;
        else if(featureMatrix.get(0).isEmpty())
            return out;
        votes = new ArrayList();
        ArrayList<FeatureElement> al;
        int pos, initRange, endRange, pRange;
        //System.out.printf("For de %d\n",inst.numAttributes());
        for(int ftIndex = 0; ftIndex < inst.numAttributes() - 1; ftIndex++){
            al = featureMatrix.get(ftIndex);
            //System.out.println("bs");
            pos = binarySearchPosition(new GetFeature(al), al.size() - 1, inst.value(inst.attribute(ftIndex)));
            initRange = (pos - getVotesRange.getValue() > 0) ? pos - getVotesRange.getValue() : 0;
            //System.out.printf("Init %d ", initRange);
            pRange = pos + getVotesRange.getValue() - (pos - initRange);
            endRange = pRange <= al.size() - 1 ? pRange : al.size() - 1;
            //System.out.printf("end %d ", endRange);
            votes.add(new ArrayList<>());
            for(int i = initRange; endRange > i; i++){
                //System.out.printf("Classe: %d Feature: %f\n",al.get(i).targetClass,al.get(i).featureValue);
                votes.get(ftIndex).add(al.get(i).targetClass);
            }
        }
        ArrayList<List<Double>> dVotesFeatures;
        dVotesFeatures = new ArrayList<>();
        List<Integer> featureVotes;
        double[] ftOut;
        for(int ftIndex = 0; ftIndex < inst.numAttributes() - 1; ftIndex++){ //Passa por feature obtendo o range
            dVotesFeatures.add(new ArrayList<>());
            ftOut = new double[inst.numClasses()];
            featureVotes = votes.get(ftIndex);
            for(int i = 0; i < featureVotes.size(); i++){ //Armazena tudo num double[]
            //    System.out.printf("Vote to %d\n", featureVotes.get(i));
                ftOut[featureVotes.get(i)] += 1.0/featureVotes.size();
            }
            //System.out.printf("Votes %d: ",ftIndex);
            //System.out.println(Arrays.toString(ftOut));

            for(int i = 0; i < inst.numClasses(); i++) //Transforma pra ArrayList<Double>
                dVotesFeatures.get(ftIndex).add(ftOut[i]);
        }
        
        //System.out.print("Votes: ");
        //System.out.println(dVotesFeatures);
        double featureQuality;
        ArrayList<ArrayList<Double>> dPonderedVotes = new ArrayList<>(); // Novo ArrayList com os votos ponderados
        for(int i = 0; i < dVotesFeatures.size(); i++){ //Medir a qualidade da feature baseado na classe predominante dos votos.
            featureQuality = Collections.max(dVotesFeatures.get(i),null)/1;
            dPonderedVotes.add(new ArrayList<>());
            for(int j = 0; j < dVotesFeatures.get(i).size(); j++){
                dPonderedVotes.get(i).add(dVotesFeatures.get(i).get(j) * featureQuality);
            }
        }
        double sumVotes = 0.0, sumFtVotes;
        for(int i = 0; i < dPonderedVotes.size(); i++)
            for(int j = 0; j < dPonderedVotes.get(i).size(); j++)
                sumVotes += dPonderedVotes.get(i).get(j);
        for(int i = 0; i < inst.numClasses(); i++){ //Calcular a média dos votos
            sumFtVotes = 0.0;
            for(int j = 0; j < dPonderedVotes.size(); j++){
                sumFtVotes += dPonderedVotes.get(j).get(i);
            }
            out[i] = sumFtVotes / sumVotes;
        }
        return out;
    }

    // Identificar a caracteristica das instâncias e modelar o algoritmo.
    public void firstUse(int numberFeatures){
        for(int i = 0; i < numberFeatures - 1; i++){
            featureMatrix.add(new ArrayList());
        }
    }
    
    
    //Retornar a posição onde melhor iria se encaixar a variavel target.
    //Utilizada para inserção otimizada e seleção do ponto do range para o getVotes.
    public int binarySearchPosition(FunctionObject vector, int end, double target ){
        int lo = 0;
        int hi = end;
        while (lo <= hi) {
            // Key is in a[lo..hi] or not present.
            int mid = lo + (hi - lo) / 2;
            if      (target < vector.get(mid)) hi = mid - 1;
            else if (target > vector.get(mid)) lo = mid + 1;
            else return mid;
        }
        return lo;
    }
    
    //Fazer o rodizio e implementar o batchSize.
    public int nextInstance(){
        nextInstance++;
        if(nextInstance == batchSize.getValue()){
            nextInstance = 0;
        }
        return nextInstance;
    }
    
    //Inserir na lista ordenado
    public void orderedInsert(ArrayList<FeatureElement> al, FeatureElement ft){

        // Remover o ultimo
        if(al.size() == batchSize.getValue()){
            int replacedIndex = ft.element_id;
            for(int i = 0; i < al.size(); i++){
                if(replacedIndex == al.get(i).element_id)
                    al.remove(i);
            }
        }
        int pos;
        if(!al.isEmpty()){
            pos = binarySearchPosition(new GetFeature(al),al.size()-1,ft.featureValue);
        }else{
            pos = 0;
        }  
        al.add(pos,ft);
        
    }

    
    @Override // Resetar classificador
    public void resetLearningImpl() {
        featureMatrix = new ArrayList();
        nextInstance = -1;
    }

    @Override // Treinar
    public void trainOnInstanceImpl(Instance inst) {
        if(featureMatrix.isEmpty()){
            firstUse(inst.numAttributes());
        }
        int inst_id = nextInstance();
        for(int ftIndex = 0; ftIndex < inst.numAttributes() - 1; ftIndex++)
            orderedInsert(featureMatrix.get(ftIndex),new FeatureElement(inst.value(inst.attribute(ftIndex)),(int) inst.classValue(), inst_id));
    }
      
    @Override // 
    protected Measurement[] getModelMeasurementsImpl() {
        return null;
    }

    @Override // Pode deixar vazio - Descrição do algoritmo
    public void getModelDescription(StringBuilder out, int indent) {
        StringUtils.appendIndented(out, indent, "Vertical Classifier ");
        out.append(this.getClassNameString());
        out.append(" ");
        out.append(featureMatrix);
        out.append(" feature bags. ");
        out.append("batchSize: ");
        out.append(batchSize.getValue());
        out.append(" featureRange: ");
        out.append(getVotesRange.getValue());
    }

    @Override
    public boolean isRandomizable() { 
        return false;
    }
    @Override
    public VerticalClassifier copy(){
        return this;
    }
    
    @Override
    public String getPurposeString() {
        return "Vertical Classifier: Create a batch ordered of each feature, and select the classes near of the instance to predict.";
    }
    
}
