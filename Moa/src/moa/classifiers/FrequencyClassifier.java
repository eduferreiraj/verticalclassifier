/*
Next updates:
- Adapt to non-numeric features (Cosine function, FeatureElement class)
- Make cosine difference weight
- Predict in the right way
*/


package moa.classifiers;
import java.util.ArrayList;
import com.yahoo.labs.samoa.instances.Instance;
import java.util.List;
import moa.core.Measurement;
import com.github.javacliparser.IntOption;
import com.github.javacliparser.StringUtils;
import static java.lang.Math.sqrt;
import java.util.logging.Level;
import java.util.logging.Logger;

// @author Eduardo Ferreira
// @email eduferreiraj@gmail.com

public class FrequencyClassifier extends AbstractClassifier {
    //DEBUG
    public void sleep(int mil){
        try {
            Thread.sleep(mil);
        } catch (InterruptedException ex) {
            Logger.getLogger(VerticalClassifier.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    //DEBUG
    
    
    public class FeatureElement {
        double featureValue;
        int targetClass;
        int element_id;
        public FeatureElement(double featureValue, int targetClass, int element_id){
            this.featureValue = featureValue;
            this.targetClass = targetClass;
            this.element_id = element_id;
        }
        @Override
        public String toString() {
            return String.valueOf(featureValue);
        }
    }
    public class InstancePointer{
        ArrayList<FeatureElement> instanceFeatures;
        boolean valid = false;
        public InstancePointer(){
            instanceFeatures = new ArrayList<>();
        }
        public void addFeature(FeatureElement feature){
            valid = true;
            instanceFeatures.add(feature);
        }
    }
    // Classe para passar uma função como parâmetro.
    public abstract class FunctionObject{
        public abstract double get(int index);
    }
    
    public class GetFeature extends FunctionObject{
        ArrayList<FeatureElement> list;
        public GetFeature(ArrayList<FeatureElement> al){
            list = al;
        }
        @Override
        public double get(int index) {
            return list.get(index).featureValue;
        }
    }
    
    
    ArrayList<ArrayList<FeatureElement>> featureMatrix;
    ArrayList<InstancePointer> originalInstance;
    public IntOption batchSize = new IntOption(
            "batchSize",
            'b',
            "The size of the batch for each feature stored.",
            200, 0, Integer.MAX_VALUE);
    public IntOption getVotesRange = new IntOption(
            "votesRange",
            'v',
            "Range of the features considered in the predict step.",
            200, 0, Integer.MAX_VALUE);
    int nextInstance = -1; 
    public class Tuple<X, Y> { 
        public final X x; 
        public final Y y; 
        public Tuple(X x, Y y) { 
          this.x = x; 
          this.y = y; 
        } 
    }
    public class ItemCounter{
        public class Item{
            int id;
            int times;
            public Item(int id){
                this.id = id;
                times = 1;
            }
            public void inc(){
                times = times + 1;
            }
            public int get(){
                return times;
            }
        }
        
        public class GetItem extends FunctionObject{
            ArrayList<Item> list;
            public GetItem(ArrayList<Item> al){
                list = al;
            }
            @Override
            public double get(int index) {
                return list.get(index).id;
            }
        }
        
        ArrayList<Item> itens; 
        public ItemCounter(){
            itens = new ArrayList<>();
        }
        public void reset(){
            itens = new ArrayList<>();
        }
        public void increment(int element){
            boolean incremented = false;
            if(itens.isEmpty()){
                itens.add(new Item(element));
                return;
            }
            int pos = binarySearchPosition(new GetItem(itens), itens.size() - 1, element);
            try{
                if(itens.get(pos).id == element){
                    incremented = true;
                    itens.get(pos).inc();
                }
            } catch(Exception e){
                
            }
            if(!incremented){
                itens.add(pos, new Item(element));
            }
        }
        
        public ArrayList<Tuple<Integer,ArrayList<Integer>>> getLists(){
            ArrayList<Tuple<Integer,ArrayList<Integer>>> list = new ArrayList<>();
            boolean flowControl;
            Tuple<Integer, ArrayList<Integer>> newTuple;
            ArrayList<Integer> newList;
            for (Item item : itens) {
                flowControl = false;
                for (Tuple<Integer, ArrayList<Integer>> itemList : list) {
                    if(itemList.x == item.times){
                        flowControl = true;
                        itemList.y.add(item.id);
                        break;
                    }
                }
                if(!flowControl){
                    newList = new ArrayList<>();
                    newList.add(item.id);
                    newTuple = new Tuple(item.times,newList);
                    list.add(newTuple);                    
                }
            }
            ArrayList<Tuple<Integer,ArrayList<Integer>>> orderedList = new ArrayList<>();
            //Sort
            int listSize = list.size();
            for(int i = 0; i < listSize; i++){
                Tuple<Integer, ArrayList<Integer>> highTuple = list.get(0);
                for( Tuple<Integer, ArrayList<Integer>> itemList : list){
                    if(highTuple.x < itemList.x){
                        highTuple = itemList;
                    } 
                }
                list.remove(highTuple);
                orderedList.add(highTuple);
            }
            return orderedList;
        }
    } 
    
   /* def coseno_peso_(x,y,pesos_y,isnumeric):
    prod = 0.0
    sum2x = 0.0
    sum2y = 0.0
    for i in range(len(x)):
        if type(x[i]) == str or type(x[i]) == np.string_ or not isnumeric[i]:
            sum2x = sum2x + 1
            sum2y = sum2y + 1
            if x[i] == y[i]:
                prod += (1 - pesos_y[i])
            else:
                prod -= (1 - pesos_y[i])               
        else:
            prod = prod + x[i] * y[i] * (1 - pesos_y[i])
            sum2x = sum2x + x[i] * x[i]
            sum2y = sum2y + y[i] * y[i]
    return prod / math.sqrt(sum2x * sum2y)*/
    public double cosine(InstancePointer instance_x, InstancePointer instance_y, ArrayList<Double> weights){
        double sumxy = 0.0;
        double sumxx = 0.0;
        double sumyy = 0.0;
        double x, y;
        for(int i = 0; i < instance_x.instanceFeatures.size(); i++){
            x = instance_x.instanceFeatures.get(i).featureValue;
            y = instance_y.instanceFeatures.get(i).featureValue;
            if(x == 0.0){
                x = 1.0;
            }
            if(y == 0.0){
                y = 1.0;
            }
            sumxy = sumxy + x * y * (1.0 - weights.get(i)); 
            sumxx = sumxx + x * x;
            sumyy = sumyy + y * y;
        }
        double cos = sumxy / sqrt(sumxx * sumyy);
        return cos;
    }
    
    public ArrayList<Double> entropy(ArrayList<List<Integer>> votes, int class_size){
        ArrayList<Double> entropy = new ArrayList<>();
        ItemCounter counter = new ItemCounter();
        double temp_entropy;
        for(List<Integer> featureRange : votes){
            temp_entropy = 0.0;
            for(Integer vote : featureRange){
                counter.increment(vote);
            }
            ArrayList<Tuple<Integer,ArrayList<Integer>>> presenceList = counter.getLists();
            for(Tuple<Integer,ArrayList<Integer>> presence : presenceList){
                for(Integer class_qnt : presence.y){
                    temp_entropy = - (double)presence.x/(double)featureRange.size();
                    temp_entropy = temp_entropy * (Math.log10((double)presence.x/(double)featureRange.size()) / Math.log10(2.));
                }
            }
            entropy.add(temp_entropy / (Math.log10((double)class_size) / Math.log10(2.)));
            counter.reset();
        }
        return entropy;
    }
    
    @Override // Testar
    public double[] getVotesForInstance(Instance inst) {
        ArrayList<List<Integer>> votes, instances_id;
        double[] out = new double[inst.numClasses()];
        for(int i = 0; i < inst.numClasses(); i++)
            out[i] = 0.0;
        
        if(featureMatrix.isEmpty()) // Se não inicializado
            return out;
        else if(featureMatrix.get(0).isEmpty()) // Se inicializado mas não treinado
            return out;
        
        instances_id = new ArrayList();
        votes = new ArrayList();
        ArrayList<FeatureElement> al;
        int pos, initRange, endRange, pRange;
        for(int ftIndex = 0; ftIndex < inst.numAttributes() - 1; ftIndex++){
            al = featureMatrix.get(ftIndex);
            pos = binarySearchPosition(new GetFeature(al), al.size() - 1, inst.value(inst.attribute(ftIndex)));
            initRange = (pos - getVotesRange.getValue() > 0) ? pos - getVotesRange.getValue() : 0; // Seta o inicio da janela
            pRange = pos + getVotesRange.getValue() - (pos - initRange); 
            endRange = pRange <= al.size() - 1 ? pRange : al.size() - 1; // Seta o final da janela
            votes.add(new ArrayList<>());
            instances_id.add(new ArrayList<>());
            for(int i = initRange; endRange > i; i++){
                votes.get(ftIndex).add(al.get(i).targetClass); // Adiciona as classes no votes
                instances_id.get(ftIndex).add(al.get(i).element_id);
            }
        }
        
        
        // Nesse momento, a matriz de inteiros votes
        // contém todas os ids do range da instacia
        // a ser predita
        
        InstancePointer instPointer = new InstancePointer(); 
        for(int i = 0; i< inst.numAttributes() - 1; i++){
           instPointer.addFeature(new FeatureElement(inst.value(inst.attribute(i)),0,0));
        }
        
        
        ArrayList<Double> weights = entropy(votes, inst.numClasses());
        ItemCounter counter = new ItemCounter();
        counter.reset();
        for(List<Integer> idPerFeature : instances_id)
            for (Integer vote : idPerFeature) {
                counter.increment(vote);
            }
        ArrayList<Tuple<Integer,ArrayList<Integer>>> presenceList = counter.getLists();

        double newCos, bestCosine = 0.0;
        int cosineCalcullated = 0;
        boolean changedBestCosine;
        int bestCosineId = 0;
        for(Tuple<Integer,ArrayList<Integer>> presence : presenceList){
            changedBestCosine = false;
            for(Integer y : presence.y){
                newCos = cosine(instPointer,originalInstance.get(y),weights);
                cosineCalcullated++;
                if(newCos > bestCosine){
                    changedBestCosine = true;
                    bestCosine = newCos;
                    bestCosineId = y;
                }
            }
            if(!changedBestCosine){
                break;
            }
        }
        
        
        out[originalInstance.get(bestCosineId).instanceFeatures.get(0).targetClass] = 1.0;
        

        return out;
    }

    // Identificar a caracteristica das instâncias e modelar o algoritmo.
    public void firstUse(int numberFeatures){
        for(int i = 0; i < numberFeatures - 1; i++){
            featureMatrix.add(new ArrayList());
        }
    }
    
    
    //Retornar a posição onde melhor iria se encaixar a variavel target.
    //Utilizada para inserção otimizada e seleção do ponto do range para o getVotes.
    public int binarySearchPosition(FunctionObject vector, int end, double target ){
        int lo = 0;
        int hi = end;
        while (lo <= hi) {
            // Key is in a[lo..hi] or not present.
            int mid = lo + (hi - lo) / 2;
            if      (target < vector.get(mid)) hi = mid - 1;
            else if (target > vector.get(mid)) lo = mid + 1;
            else return mid;
        }
        return lo;
    }
    
    //Fazer o rodizio e implementar o batchSize.
    public int nextInstance(){
        nextInstance++;
        if(nextInstance == batchSize.getValue()){
            nextInstance = 0;
        }
        return nextInstance;
    }
    
    //Inserir na lista ordenado
    public void orderedInsert(ArrayList<FeatureElement> al, FeatureElement ft){

        // Remover o ultimo
        if(al.size() == batchSize.getValue()){
            int replacedIndex = ft.element_id;
            for(int i = 0; i < al.size(); i++){
                if(replacedIndex == al.get(i).element_id)
                    al.remove(i);
            }
        }
        int pos;
        if(!al.isEmpty()){
            pos = binarySearchPosition(new GetFeature(al),al.size()-1,ft.featureValue);
        }else{
            pos = 0;
        }  
        al.add(pos,ft);
        
    }

    
    @Override // Resetar classificador
    public void resetLearningImpl() {
        featureMatrix = new ArrayList();
        nextInstance = -1;
        originalInstance = new ArrayList();
        for(int i = 0; i < batchSize.getValue(); i++){
            originalInstance.add(new InstancePointer());
        }
    }

    @Override // Treinar
    public void trainOnInstanceImpl(Instance inst) {
        if(featureMatrix.isEmpty()){
            firstUse(inst.numAttributes());
        }
        FeatureElement ft;
        int inst_id = nextInstance();
        for(int ftIndex = 0; ftIndex < inst.numAttributes() - 1; ftIndex++){
            ft = new FeatureElement(inst.value(inst.attribute(ftIndex)),(int) inst.classValue(), inst_id);
            originalInstance.get(inst_id).addFeature(ft);
            orderedInsert(featureMatrix.get(ftIndex), ft);
        }
    }
      
    @Override // 
    protected Measurement[] getModelMeasurementsImpl() {
        return null;
    }

    @Override // Pode deixar vazio - Descrição do algoritmo
    public void getModelDescription(StringBuilder out, int indent) {
        StringUtils.appendIndented(out, indent, "Vertical Classifier ");
        out.append(this.getClassNameString());
        out.append(" ");
        out.append(featureMatrix);
        out.append(" feature bags. ");
        out.append("batchSize: ");
        out.append(batchSize.getValue());
        out.append(" featureRange: ");
        out.append(getVotesRange.getValue());
    }

    @Override
    public boolean isRandomizable() { 
        return false;
    }
    @Override
    public FrequencyClassifier copy(){
        return this;
    }
    
    @Override
    public String getPurposeString() {
        return "Vertical Classifier: Create a batch ordered of each feature, and select the classes near of the instance to predict.";
    }
    
}
