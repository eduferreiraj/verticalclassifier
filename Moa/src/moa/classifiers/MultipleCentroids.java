package moa.classifiers;

import java.io.Serializable;
import java.util.ArrayList;
import com.yahoo.labs.samoa.instances.Instance;
import moa.core.Measurement;
import com.github.javacliparser.IntOption;
import static java.lang.Math.sqrt;
import java.util.logging.Level;
import java.util.logging.Logger;

// @author Eduardo Ferreira
// @email eduferreiraj@gmail.com

public class MultipleCentroids extends AbstractClassifier {
    
        //DEBUG
    public void sleep(int mil){
        try {
            Thread.sleep(mil);
        } catch (InterruptedException ex) {
            Logger.getLogger(VerticalClassifier.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    //DEBUG
    
    public IntOption maxCentroids = new IntOption(
        "maxCentroids",
        'm',
        "Maximum of centroids permitted to classify.",
        60, 0, Integer.MAX_VALUE);

    
    public class CentroidsController implements Serializable{
        int refreshed, improved;
        @Override
        public String toString(){
            StringBuilder ret_str = new StringBuilder();            
            ret_str.append("CentroidsController: \n");
            for(ArrayList<Centroid> class_cent : multipleCentroids){
                ret_str.append("(");
                ret_str.append(class_cent.size());
                ret_str.append(")");
                
                for(Centroid c : class_cent){
                    ret_str.append(c.toString());
                }
                ret_str.append("\n");
            }
            
            
            return ret_str.toString();
            
        }
        
        ArrayList<ArrayList<Centroid>> multipleCentroids;
        public CentroidsController(){
             multipleCentroids = new ArrayList();
        }
        public void train(Instance inst){
            Centroid centroid = bestCentroid(inst);
            if(centroid == null){
                refreshed++;
                //System.out.println("Refreshed");
                refreshCentroids(inst);
            }
            else if(centroid.target_class != (int) inst.classValue()){
                refreshed++;
                //System.out.print("Refreshed: ");
                //System.out.print(centroid.target_class);
                //System.out.print(" ");
                //System.out.println((int) inst.classValue());
                refreshCentroids(inst);
            }else{
                improved++;
                //System.out.println("Improved");
                centroid.improve(inst);
            }
            StringBuilder str = new StringBuilder();
            //str.append("Refreshed: ");
            //str.append(refreshed);
            //str.append(" Improved: ");
            //str.append(improved);
            //System.out.println(str.toString());
            //sleep(100);
        }
        public double[] compare(Instance inst){
            double[] predicta = new double[inst.numClasses()];
            Centroid centroid = bestCentroid(inst);
            if(centroid == null){
                predicta[0] = 1.0;
                return predicta;
            }
            predicta[centroid.target_class] = 1.0;
            return predicta;
        }
        public Centroid bestCentroid(Instance inst){
            initialize(inst.numClasses());
            double bestDistance = 0, t_distance = Double.MAX_VALUE;
            Centroid bestCentroid = null;
            ArrayList<Double> instanceToDouble = new ArrayList();
            for(int i=0; i<inst.numAttributes(); i++){
                instanceToDouble.add(inst.value(inst.attribute(i)));
            }
            for(ArrayList<Centroid> classCents : multipleCentroids){
                for(Centroid centroid : classCents){
                    t_distance = centroid.getDistance(instanceToDouble);
                    if(t_distance > bestDistance){
                        bestDistance = t_distance;
                        bestCentroid = centroid;                                
                    }
                }
            }
            return bestCentroid;
        }
        
        public void initialize(int numClass){
            if(multipleCentroids.isEmpty())
                for(int i = 0; i < numClass; i++)
                    multipleCentroids.add(new ArrayList());
        }
        
        public void refreshCentroids(Instance inst){
            initialize(inst.numClasses());
            ArrayList<Double> attr = new ArrayList();
            for(int i = 0; i<inst.numAttributes(); i++)
                attr.add(inst.value(inst.attribute(i)));
            if(multipleCentroids.get((int) inst.classValue()).isEmpty()){
                add((int)inst.classValue(), attr);
                return;
            }
            double minDistance = Double.MAX_VALUE, temp = 0.0;
            int firstCentroid = 0, secondCentroid = 0, class_cent = -1;
            ArrayList<Centroid> class_centroids;
            for(int c = 0; c < multipleCentroids.size(); c++){
                class_centroids = multipleCentroids.get(c);
                for(int i = 0; i < class_centroids.size(); i++){
                    for(int j = i; j < class_centroids.size(); j++){
                        if(j == i)
                            continue;
                        temp = class_centroids.get(i).getDistance(class_centroids.get(j).centroidValues);
                        
                        if(temp < minDistance){
                            minDistance = temp;
                            firstCentroid = i;
                            secondCentroid = j;
                            class_cent = c;
                        }       
                    }
                }
            }
            int sum = 0;
            for(ArrayList<Centroid> c : multipleCentroids){
                sum = sum + c.size();
            }
            if(sum >= maxCentroids.getValue()){
                class_centroids = multipleCentroids.get(class_cent);
                class_centroids.get(firstCentroid).merge(class_centroids.get(secondCentroid));
                class_centroids.remove(secondCentroid);
            }
            add((int)inst.classValue(),attr);
        }
        public void add(int tclass, ArrayList<Double> attr){
            multipleCentroids.get(tclass).add(new Centroid(attr, tclass));
        }

    }
           
    public class Centroid implements Serializable {
        int times = 1;
        int target_class;
        @Override
        public String toString(){
            StringBuilder str = new StringBuilder();
            str.append("[");
            str.append(times);
            str.append("]");
            return str.toString();
        }
        public Centroid(ArrayList<Double> attr, int target){
            this.target_class = target;
            centroidValues = attr;
        }
        ArrayList<Double> centroidValues = new ArrayList();
        public void improve(Instance inst){
            ArrayList<Double> tempValues = new ArrayList();
            for(int i = 0; i < inst.numAttributes(); i++){
                tempValues.add((inst.value(inst.attribute(i)) + (centroidValues.get(i)*times))/times+1);
            }
            times++;
            centroidValues = tempValues;
        }
        public void merge(Centroid cent){
            ArrayList<Double> tempValues = new ArrayList();
            for(int i = 0; i < centroidValues.size(); i++){
                tempValues.add((centroidValues.get(i) + cent.centroidValues.get(i))/2);
            }
            times = (times + cent.times)/2;
            centroidValues = tempValues;
        }
        public double getDistance(ArrayList<Double> list){
            return cosine(list);
        }
        public double cosine(ArrayList<Double> list){
            double sumxy = 0.0;
            double sumxx = 0.0;
            double sumyy = 0.0;
            double x, y;
            for(int i = 0; i < list.size(); i++){
                x = centroidValues.get(i);
                y = list.get(i);
                if(x == 0.0){
                    x = 1.0;
                }
                if(y == 0.0){
                    y = 1.0;
                }
                sumxy = sumxy + x * y; 
                sumxx = sumxx + x * x;
                sumyy = sumyy + y * y;
            }
            double cos = sumxy / sqrt(sumxx * sumyy);
            return cos;
        }
    }
    
    CentroidsController cc = new CentroidsController(); 
    
    @Override
    public double[] getVotesForInstance(Instance inst) {
        //System.out.println("Classify");
        //System.out.println(cc.toString());
        return cc.compare(inst);
    }

    @Override
    public void resetLearningImpl() {
        cc = new CentroidsController();
    }

    @Override
    public void trainOnInstanceImpl(Instance inst) {
        //System.out.println("Train");
        //System.out.println(cc.toString());
        
        cc.train(inst);
    }

    @Override
    protected Measurement[] getModelMeasurementsImpl() {
        return null;
    }

    @Override
    public void getModelDescription(StringBuilder out, int indent) {
    }

    @Override
    public boolean isRandomizable() {
        return false;
    }
    
    @Override
    public MultipleCentroids copy(){
        return this;
    }
   
}
